<?php

use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('index', function () {
    // $user = [];
    $user = factory(User::class, 10)->make();
    return view('stater', compact('user'));
});
Route::get('user/create', function () {
    return view('users.create');
})->name('user.create');
Route::post('user/store', function () {
    $data = request()->all();
    $params = \Arr::except($data, ['_token', 'cfPassword']);
    User::create($params);
    return Redirect(route('users.index'));
})->name('user.store');
Route::get('users', function () {
    $data = User::All();
    return view('users.index', compact('data'));
})->name('users.index');
Route::get('users/edit/{user}', function ($id) {
    $data = User::find($id);
    return view('users.edit', compact('data'));
})->name('users.edit');
Route::post('users/save', function () {
    $id = request()->id;
    $data = [
        'name' => request()->name,
        'email' => request()->email,
    ];
    User::where('id', $id)->update($data);
    return Redirect()->route('users.index');
})->name('user.update');
Route::post('users/destroy/{user}', function (User $user) {
    User::destroy($user['id']);
    return Redirect()->route('users.index');
})->name('user.destroy');
