@extends('layout')

@section('content')
<div class="content-wrapper">
<form method="POST" action="{{route('user.update',['id' =>$data->id])}}">
    @csrf
    <div>
        <input type="hidden" name="id" value="{{ $data->id }}">
        <label for="name">Name</label>
        <input type="text" name="name" value="{{ $data->name }}" class="form-control" />
    </div>
    <div>
        <label for="name">Email</label>
        <input type="text" name="email" value=" {{ $data->email }}" class="form-control" />
    </div>
    <div>
        {{-- <label for="name">Password</label> --}}
        {{-- <input type="hidden" name="password" {{ $data->password }} class="form-control" /> --}}
    </div>
    {{-- <div>
        <label for="name">Confrirm Password</label>
        <input type="password" name="cfPassword" class="form-control" />
    </div> --}}
    <div>
        <button class="btn btn-primary my-2" type="submit" >Save</button>
    </div>
</form>
</div>
@endsection