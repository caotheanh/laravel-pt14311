@extends('layout')
@section('content')
<div class="content-wrapper">
    <a href="{{ route('user.create') }}" class="btn btn-primary">Them moi</a>
    @if(count($data)>0)
      <table class="table">
        <thead>
          <th>id</th>
          <th>Name</th>
          <th>Eame</th>
          <th>Action</th>
        </thead>
        <tbody>
         
          @foreach($data as $row)
          <tr>
          <td>{{ $row->id }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->email }}</td>
            <td>
                <a href="{{ route('users.edit', ['user'=>$row->id]) }}" class="btn btn-primary">Edit</a>
                <form method="POST" action="{{ route('user.destroy',['user' =>$row->id]) }}">
                    @csrf
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
          </tr>
          @endforeach
          
        </tbody>
      </table>
      @else
          <p>Khong co data</p>
          @endif
    </div>
@endsection